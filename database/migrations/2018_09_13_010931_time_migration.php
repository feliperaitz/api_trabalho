<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimeMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('times', function(Blueprint $a){
            $a->increments('id');
            $a->string('descricao');
            $a->integer('pontos_time');
            $a->integer('id_jogador')->unsigned();
            $a->foreign('id_jogador')->references('id')->on('jogadors');
            $a->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('times');
    }
}
