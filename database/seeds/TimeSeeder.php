<?php

use Illuminate\Database\Seeder;
use App\Time;
class TimeSeeder extends Seeder
{
    
    public function run()
    {
        Time::create([
        	'descricao' => 'Barcelona',
        	'pontos_time' => 59,
        	'id_jogador' => 3
        ]);

        Time::create([
        	'descricao' => 'Gremio',
        	'pontos_time' => 41,
        	'id_jogador' => 4
        ]);

        Time::create([
        	'descricao' => 'Atletico de madri',
        	'pontos_time' => 69,
        	'id_jogador' => 5
        ]);

        Time::create([
        	'descricao' => 'Liverpool',
        	'pontos_time' => 83,
        	'id_jogador' => 1
        ]);

        Time::create([
        	'descricao' => 'Palmeiras',
        	'pontos_time' => 35,
        	'id_jogador' => 2
        ]);
    }
}
