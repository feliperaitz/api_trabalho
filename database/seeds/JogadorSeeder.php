<?php

use Illuminate\Database\Seeder;
use App\Jogador;


class JogadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jogador::create([
        		'nome' => 'Pedro',
        		'idade' => 25,
        		'melhor_pe' => 'destro'
        ]);

        Jogador::create([
        		'nome' => 'Felipe',
        		'idade' => 18,
        		'melhor_pe' => 'destro'
        ]);

        Jogador::create([
        		'nome' => 'Camila',
        		'idade' => 22,
        		'melhor_pe' => 'canhota'
        ]);

     	Jogador::create([
        		'nome' => 'Leonardo',
        		'idade' => 19,
        		'melhor_pe' => 'canhoto'
        ]);
        
        Jogador::create([
        		'nome' => 'Olivia',
        		'idade' => 28,
        		'melhor_pe' => 'destro'
        ]);   	
    }
}
