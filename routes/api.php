<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('/estados', 'EstadoController');

Route::resource('/estadosDB', 'EstadoDbController');

Route::resource('/cidadeDB', 'CidadeDB');

Route::resource('/cidades', 'CidadeController');

Route::resource('/jogadores', 'JogadorController');

Route::resource('/times', 'TimeController');

Route::resource('/jogadores/editar','JogadorController');

Route::get('/cep', function (){
	$dados = array(
		"cep" => "85660000",
		"cidade" => "Dois Vizinhos",
		"uf" => "PR"
 	);
	return $dados;
});


Route::get('/funcionarios', function (){

		$dados = array(
			"funcionarios" => [
				array(
					"nome" => "Joao da silva",
					"idade" => 12
				),
				array(
					"nome" => "Maria da Oliveira",
					"idade" => 20
				
				)
			]
		);
			return response()->json($dados);
			
});