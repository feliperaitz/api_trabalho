<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $fillable = ['cidade','id_estado'];

    public $timestamps = false;

    public function estado(){

    	return $this->belongsTo('App\Estado', 'id_estado', 'id');
    }
}
