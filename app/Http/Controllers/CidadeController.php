<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Cidade;
class CidadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Cidade::all();

        try{
        return response()->json( [Cidade::all()], 200);
         }catch (/Exception $e){
            return response()->json(["mensagens" => $e->getMessage()], 400);
         }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'cidade' => 'required|min:3',
                'id_estado' => 'required'
            ]
        );

        $cidade = new Cidade();

        $cidade->fill($request->all());
        $cidade->save();

        return $cidade;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cidade = Cidade::find($id);
        return $cidade;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cidade = Cidade::find($id);
        $cidade->fill($request->all());
        $cidade->save();

        return $cidade;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cidade = Cidade::find($id);
        $cidade->delete();
        return $cidade;
    }
}
