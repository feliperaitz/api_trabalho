<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Time;

class TimeController extends Controller
{
 
    public function index(Request $request)
    {
        $qtd = $request->input('qtd');
        return Time::paginate($qtd);
            }

    public function store(Request $request)
    {
        $time = Time::with('times')->find('id');
        return $time;
    }


    public function show($id)
    {
        $time = Time::find($id);
        return $time;
    }

    
    public function update(Request $request, $id)
    {
        $time = Time::find($id);
        $time->fill($request->all());
        $time->save();

        return $time;
    }

    public function destroy($id)
    {
        $time = Time::find($id);
        $time->delete();
        return $time;
    }
}