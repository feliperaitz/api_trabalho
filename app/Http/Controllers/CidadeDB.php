<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CidadeDB extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cidades = DB::select('select * from cidades');

        return $cidades;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           DB::insert('insert into cidades (cidade, id_estado) values (?,?)', [$request->input('cidade'), $request->input('id_estado')]);

        return "Inserido com sucesso";
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           $cidades = DB::select('select * from cidades where id = ? ', [$id]);

        //return "Contoller DB, nao ira utilizar model";

        return $cidades;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cidades = DB::update('update cidades set cidade = ?, id_estado = ? where id = ? ',
            [$request->input('cidade'), $request->input('id_estado'), $id]);

        return $cidades;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
         DB::delete('delete from cidades where id = ?');

    }
}
