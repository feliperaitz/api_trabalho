<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Jogador;
class JogadorController extends Controller
{
    
    public function index(Request $request)
    {

          /* try{
        return response()->json( [Cidade::all()], 200);
         }catch (/Exception $e){
            return response()->json(["mensagens" => $e->getMessage()], 400);
         }*/
      try{
        $qtd = $request->input('qtd');
        return Jogador::paginate($qtd);
   }catch(\Exception $e){

    echo "Erro no metodo index da Controller";
   }
 }
 public function store(Request $request)
    {
       // $this->validate($request->only(['cidade', 'id_estado']),[]);
        try{
        $jogador = Jogador::with('jogadors')->find('id');
        return $jogador;
        echo "Inserido com sucesso";
    }catch(\Exception $e){
       return "Problemas no metodo store Jogador Controller";
    }
    }

    public function show($id)
    {   
        try{
        $jogador = Jogador::find($id);

        return $jogador;
    }catch(\Exception $e){
        return "Erro com o metodo show do Jogador Controller";
    }
    }

 
    public function update(Request $request, $id)
    {
        try{
        $jogador = Jogador::find($id);
        $jogador->fill($request->all());
        $jogador->save();

        return $jogador;
    }catch(\Exception $e){
        return "Erro com o metodo Update no Jogador Controller";
    }
    }

  
    public function destroy($id)
    {   
        $jogador = Jogador::find($id);
        $jogador->delete();
        return "Registro excluido com sucesso";
     
    }
}
