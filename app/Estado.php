<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $fillable = ['descricao', 'uf'];



    public function cidades(){

    	return $this->belongsTo('App\Cidade', 'id_estado', 'id');
    }
}
